#!/usr/bin/python3

import argparse
import os
import configparser
import subprocess
import re

IMAGEMAGICK = "convert"


def templatize_png_file(filepath, out_filepath, vars):
    alpha = vars.get("png_alpha_active", "0.5")
    if "inactive" in filepath:
        alpha = vars.get("png_alpha_inactive", "0.5")
    # Imagemagick's "alpha" channel is opacity.
    im_alpha = min(1, max(0, float(alpha)))
    cmd = [
        "convert",
        filepath,
        "-alpha", "on",
        "-channel", "alpha",
        "-fx", f"(u>0) ? {im_alpha:.3f} : 0",
        out_filepath,
    ]
    # print(cmd)
    subprocess.check_call(cmd)


def templatize_text_file(filepath, out_filepath, vars):
    with open(filepath, "r") as f_in:
        buf = f_in.read()
        for n, v in vars.items():
            # If a raw hex colour is specified,
            # preferably remove XPM symbolic refs ("#123abc s sym_name"),
            # and sub the raw hex colour for the original colour.
            if v.startswith("#"):
                col_and_subst_re = re.compile(
                    r'#[0-9a-f]{6}\s+s\s+@' + re.escape(n) + '@',
                    re.I,
                )
                buf = col_and_subst_re.sub(v, buf)

            # Always replace our template tokens in
            # the normal, non-XPM-aware way.
            nt = f"@{n}@"
            buf = buf.replace(nt, v)
        with open(out_filepath, "w") as f_out:
            f_out.write(buf)


def templatize_src_tree(src_dir, name, vars, output_root):
    print(f"Writing {output_root}/{name}…")
    for dirpath, dirnames, filenames in os.walk(src_dir):
        dirpath_rel = os.path.relpath(dirpath, src_dir)
        for filename in filenames:
            if filename.startswith("_"):
                continue
            filepath = os.path.join(dirpath, filename)
            filepath_rel = os.path.join(dirpath_rel, filename)
            out_filepath = os.path.join(output_root, name, filepath_rel)
            out_dirpath = os.path.join(output_root, name, dirpath_rel)
            os.makedirs(out_dirpath, exist_ok=True)
            if filepath.endswith(".png"):
                templatize_png_file(filepath, out_filepath, vars)
            else:
                templatize_text_file(filepath, out_filepath, vars)


def main():
    args = get_args()
    if not os.path.isdir(args.src_dir):
        raise RuntimeError(f"No src tree named {args.src_dir}")
    config = configparser.ConfigParser()
    config.read_file(args.vars_inifile)
    for sec in config.sections():
        name = sec
        vars = dict(config.items(sec))
        vars["variant_name"] = sec
        templatize_src_tree(args.src_dir, name, vars, args.output_root)


def get_args():
    parser = argparse.ArgumentParser(
        description="""
            Templatize trees of text files according to a
            definitions file.
        """,
    )
    parser.add_argument(
        "src_dir",
        type=str,
        help="The source tree",
    )
    parser.add_argument(
        "vars_inifile",
        type=argparse.FileType("r"),
        help="DOS-style INI file containing sections and variables",
    )
    parser.add_argument(
        "output_root",
        type=str,
        help="The output root"
    )
    return parser.parse_args()


if __name__ == "__main__":
    main()
