PROJECT = Marwaita-Xfwm
THEMES_DIR ?= ${HOME}/.themes

all: build

.PHONY: clean build install uninstall

build:
	mkdir -p themes
	./colorschemes.py src colorschemes.ini themes
	# Optionally anonymize the generated .png files.
	# This makes the diffs more predictable.
	find themes -type f -name '*.png' -exec mat2 --inplace {} + || true

clean:
	rm -fr themes/*
	find . -iname '*~' -type f -delete

uninstall:
	rm -fr "${THEMES_DIR}/${PROJECT}"*

install:
	@echo "Installing into ${THEMES_DIR}…"
	for a in themes/*; do \
	  echo "Installing $$a"; \
	  b=`basename "$$a"`; \
	  rm -fr "${THEMES_DIR}/$$b"; \
	  cp -a "$$a" "${THEMES_DIR}/$$b"; \
	done
