# ![](docs/Marwaita-Xfwm-64.svg) Marwaita themes for Xfwm4

*Marwaita for Xfwm* (or *Marwaita-Xfwm* for short) is a set of themes
for Xfce's window manager. The themes here are a third party addition to
the excellent [Marwaita][marwaita] Cinnamon+GTK themes made by
[darkomarko42][darkomarko42]. The main Marwaita project is a minimal and
modern desktop theme that supports a particularly wide spread of desktop
environments and widget library versions, but not Xfce out of the box!

The _Marwaita-Xfwm_ themes complement the stock _Marwaita_ themes, and
extend them with full support for Xfce. They work a little differently
in the theme selectors, so the theme names are a bit different too.

![Busy screenshot showing these Xfwm themes in use](docs/Screenshot-256.jpg)

## Features

* Big, accessible resize handles 📐
* Matches the official Marwaita CSD theme perfectly 🪞
* Picks up the GTK theme's colors, tailored for Marwaita 👔
* Supports fullscreen and shade states correctly 🪗
* Not opinionated or fancy, just nice 🧸

### Support

* Supports all recent versions of Xfce
* Best with Xfwm 4.12.1 and above
* Supports all the OS branded colour editions of Marwaita 🌈
* Matches all the regular, `Dark`, and `Alt`-titlebar variants too! 🦫
* No support for `Marwaita OSX` just yet 😢 (patches welcome!)
* Supports the colored title bar  with `Color` `Color Dark` themes,
  with slightly imperfect titlebar text colors

## Documentation

* [User Guide][userguide]
* [Developer Notes][devnotes]

## Links

* [Marwaita for Xfwm's listing on xfce-look.org][xfcelook]

[userguide]: docs/USER.md
[devnotes]: docs/DEVELOPER.md
[marwaita]: https://www.pling.com/c/1459536
[darkomarko42]: https://github.com/darkomarko42
[xfcelook]: https://www.xfce-look.org/p/1877124/
