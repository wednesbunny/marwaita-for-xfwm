# ![](Marwaita-Xfwm-64.svg) Developing Marwaita for Xfwm

This document describes how to build and maintain the [Marwaita for
Xfwm][README] themes. Most people using the themes won't have to follow
these instructions.

See the [main README][README] for a general project overview. Read the
[User Guide][userguide] instead if you just want to install the themes
on your computer and use them. The notes below are only needed for
rebuilding certain files that are distributed with this theme, and for
people who want to contribute back or understand how this stuff works in
more detail.

# Prerequisites

If you are building the theme, you will need:

* [ImageMagick][imagemagick]'s `convert` command,
  for PNG file alpha manipulation.
* [The Metadata anonymisation toolkit v2][mat2] (and its `mat2` command),
  for PNG file cleanup and build reproducibility.
* [Python3][py3]
* [GNU Make][make], or any compatible `make` tool

On a Debian system, you can use the following to install the build
dependencies

```shell
sudo apt install -y imagemagick mat2 python3 make
```

# Working with the source

To clone the theme source in order to play around with it, use

```shell
cd path/to/my/src
git clone https://gitlab.com/wednesbunny/marwaita-for-xfwm.git
```

The primary focus of development is the branch named `main`.

## Building and releasing

The Makefile supports the usual `build`, `install`, and `clean` targets,
plus an `uninstall` target. Override `THEMES_DIR` to install to a
different install location. `PREFIX` and other standard vars are not
supported.

```shell
# Rebuild, then install into ~/.themes
cd path/to/marwaita-for-xfwm
make clean
make build
make install

# Rebuild and install into /usr/local
export THEMES_DIR=/usr/local/share/themes
make build install

# Undo the above (ensure THEMES_DIR is set appropriately)
make uninstall
```

For releases, the latest rebuild of `themes/` is included. At present, I
don't have a policy about whether to include that at release time, in
the same commit as each change, or in a change immediately following it.

This is because people following themes via git with symlinks expect the
rebuilt themes to be present in the source checkout.

## Build process and output

The list of generated themes, and the colours and alpha values used in
them are controlled by [`colorschemes.ini`](../colorschemes.ini) in the
main project directory. Hopefully the `colorschemes.py` build script is
straightforward and understandable, and the `.ini` file contains
enough documentation to get you going.

Each section in `colorshcmes.ini` defines the parameters for a theme.
The build scripting converts the `src/` template tree into multiple
output themes, one per section in. It works via template substitution
for XPM files in `src` and the main `themerc`. PNG source files are
converted by changing their alpha: they operate as overlays on top of
the XPM images.

See the [Xfwm theme how-to][xfwmhowto] guide for more details of how the
output themes work.

## Contributing changes

Better still, contribute your changes back. Start by making your own
personal fork of the codebase, clone that and follow [Gitlab's User
Guide][gitlabguide] to figure out the rest. I'm always happy to receive
[merge requests][mr]!

# Reporting bugs 🪰

Please use the [Gitlab issue tracker][bugs] to report bugs. I can't
guarantee an instant response, but that's the best place to report it do
it doesn't get forgotten.

# Design notes

Window edges are fairly big and easy to grip without sacrificing the
[Marwaita][marwaita] look. Corners are big "L" shapes, and the vertical
and bottom borders are 6 pixels wide. That's smaller than Marwaita CSD
windows, but I hope it's still usable.

The theme has a very slight bias towards windows with proper menu bars,
where the window title bar runs into the menu bar.  All possible Xfce4
window buttons are included. A nice feature of this bias towards menu
bars is that it also looks slightly better when a window is shaded. The
extra transparent pixels above the buttons help position them in a
slightly more central position on the rolled-up window.

The close, maximize, and hide buttons don't show any toggle states, for
neatness and consistency with Marwaita CSD windows. It's expected that
this is the normal set of buttons that you'll use together on the same
side of the window. Note that Marwaita doesn't reveal toggle states for
its maximize button on CSD windows. Conversely the remaining three
buttons (menu, stick, shade) do reveal their toggled state similarly to
Marwaita buttons in CSD headerbars. It's expected that you'll cluster
these together over on the other side of the window.

When a window is maximized, the resize borders disappear to make maximum
use of the screen real estate for content. This includes the top resize
edge. The buttons are aligned against the edge of the screen when the
window is maximized, making use of [Fitts's law][fitts] to make them
easier to hit with the mouse.

[README]: ../README.md
[userguide]: USER.md
[marwaita]: https://www.pling.com/c/1459536
[fitts]: https://en.wikipedia.org/wiki/Fitts's_law#Implications_for_UI_design
[mat2]: https://0xacab.org/jvoisin/mat2
[imagemagick]: https://imagemagick.org/
[make]: https://www.gnu.org/software/make/
[py3]: https://www.python.org/
[bugs]: https://gitlab.com/wednesbunny/marwaita-for-xfwm/-/issues
[gitlabguide]: https://docs.gitlab.com/ee/user/index.html
[mr]: https://docs.gitlab.com/ee/user/project/merge_requests/index.html
[xfwmhowto]: https://wiki.xfce.org/howto/xfwm4_theme

