# ![](Marwaita-Xfwm-64.svg) Using Marwaita for Xfwm

This document contains full details of how to get the Marwaita for Xfwm
themes working on your Xfce desktop, and instructions for using them.

See the [main README][README] for an overview of the themes. If you want
to modify Marwaita for Xfwm or help make it look even better, please
read the [Developer Notes][devnotes] as well.

## Installation

You should install a recent Cinnamon+GTK theme from the [Marwaita
Collection][marwaita] first, and get it working. You don't need to
install Cinnamon for this step.

Next, download this project as a `.tar.gz` archive (*NOT* `.zip`) from
its [release tags page][releases] page. Or download the [latest
prerelease version][prerelease] from gitlab. These "tarball" archives
can be unpacked with _xarchiver_, _File Roller_, or most other graphical
archiver apps.

Once `marwaita-for-xfwm` is unpacked on your computer, the simplest way
to install the themes is to copy the directories from inside the
[themes](../themes/) subfolder into your personal themes folder. To
locate your personal themes folder, open `~/.themes` in Thunar. Please
create it first if it's missing.

There are currently 5 Marwaita for Xfwm themes, made to suit different
variants of the main Marwaita themes.

### Expert installation methods

Expert users can use _git_ to clone this repository somewhere unchanging
and safe, and symlink those folders into your `~/.themes` instead. Or
use the `make install` commands below.

To install from a Git clone, or from a release tarball, you can use

```shell
# Personal install
cd path/to/marwaita-for-xfwm
make install

# System-wide install
cd path/to/marwaita-for-xfwm
export THEMES_DIR=/usr/local/share/themes
sudo make install
```

This will work without having to rebuild the theme first.

> 💡Tip: use `make uninstall` for quick removal.

## Runtime dependencies

* A recent [Marwaita Cinnamon/GTK][marwaita] theme
* Xfce 4.12 or later

See the [Developer Notes][devnotes] for a fuller list of dependencies
for actually rebuilding the theme. Most people won't need to do that,
though.

## Configure Xfce

Once the theme is installed,

1. Use the Appearance control panel in Xfce's Settings app to select a
   Marwaita GTK theme.

2. Then use the Window Manager control panel to choose a Marwaita-Xfwm
   theme to match. See the table below for details.

## Matching Marwaita Xfwm and GTK themes

Marwaita has a lot of themes for different OS colours, and it can be
confusing to make Xfwm match the GTK theme. The recommended matchup is

| **Marwaita Xfwm theme**        | **Marwaita GTK**  | **Notes**        |
| -------------------------      | ----------------- | ---------------- |
| Marwaita-Xfwm                  | Marwaita *        |                  |
| Marwaita-Xfwm-Alt              | Marwaita Alt *    |                  |
| Marwaita-Xfwm-Color-Dark-Text  | Marwaita Color *  | dark title text  |
| Marwaita-Xfwm-Color-Light-Text | Marwaita Color *  | light title text |
| Marwaita-Xfwm-Dark             | Marwaita Dark *   |                  |

Where "\*" stands for one of the operating system names supported by
Marwaita GTK, such as "Debian" or "Zorin", or nothing at all.

[README]: ../README.md
[devnotes]: DEVELOPER.md
[marwaita]: https://www.pling.com/c/1459536
[releases]: https://gitlab.com/wednesbunny/marwaita-for-xfwm/-/tags
[prerelease]: https://gitlab.com/wednesbunny/marwaita-for-xfwm/-/archive/main/marwaita-for-xfwm-main.tar.gz
